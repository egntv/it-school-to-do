// aici va treceti toti providerii (toate contextele)
// de ex: UserProvider, TodosProvider etc...

// oriunde o sa vedeti children ca parametru la componente
// children reprezinta alte componente pasate in aceasta componenta
// pentru a fi afisate in interiorul ei

// children trebuie sa fie wrap-uiti de toate providerele ca sa aiba
// acces la ce este in ele

import UserProvider from "./UserProvider";
import TodosProvider from "./TodosProvider";

const Providers = ({ children }) => {
  return (
    <UserProvider>
      <TodosProvider>{children}</TodosProvider>
    </UserProvider>
  );
};

export default Providers;

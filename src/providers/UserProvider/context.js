import React from "react";

// aici cream contextul pentru user si definim valorile initiale pe
// care variabilele/functiile din contextul userului o sa le aiba

export const UserContext = React.createContext({
  users: [],
  setUsers: () => undefined,
  deleteUser: () => undefined,
  // + alte functii de modificare/procesare a userului
  // pe care mai vreti sa le expuneti catre toata aplicatia
});

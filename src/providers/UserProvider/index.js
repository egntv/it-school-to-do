// acesta este provider-ul user-ului, aici va definiti user-ul si
// toate functiile pe care vreti sa le expuneti aplicatiei
import { useState } from "react";
import { UserContext } from "./context";

const UserProvider = ({ children }) => {
  const [users, setUsers] = useState([]);

  const deleteUser = () => {
    const response = fetch(`[INSERT_URL]/${users?.[0]?.id}`, {
      method: "DELETE",
    });
  };

  const value = {
    users,
    setUsers,
    deleteUser,
  };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};

export default UserProvider;

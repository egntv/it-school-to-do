// acesta este provider-ul user-ului, aici va definiti user-ul si
// toate functiile pe care vreti sa le expuneti aplicatiei
import { useEffect, useMemo, useState } from "react";
import { TodosContext } from "./context";

const TodosProvider = ({ children }) => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    getTodos();
  }, []);

  const getTodos = () => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((parsedResponse) => {
        setTodos(parsedResponse);
      });
  };

  const value = useMemo(
    () => ({
      todos,
      setTodos,
    }),
    [todos]
  );

  return (
    <TodosContext.Provider value={value}>{children}</TodosContext.Provider>
  );
};

export default TodosProvider;

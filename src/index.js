import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import Router from "./router";
import Providers from "./providers";

// este important sa faceti wrap la toata aplicatia, in cazul nostru la Router
// cu <Providers>...</Providers>, iar in cazul de fata,
// proprietatea de 'children' pe care o primeste componenta de Providers
// este chiar Router, yeeeeeeey

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Providers>
      <Router />
      <div>
        <div></div>
      </div>
    </Providers>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

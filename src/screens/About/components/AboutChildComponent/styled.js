import styled from "styled-components";

export const Components = {
  StyledDiv: styled.div``,

  StyledButton: styled.button``,

  StyledParagraph: styled.p``,
};

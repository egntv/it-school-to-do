import { useState } from "react";
import { Components } from "./styled";

const AboutChildComponent = ({ callback }) => {
  const [childState, setChildState] = useState("stateToBeLifted");

  // acesta e un onClick handler (vezi cursul trecut) care apeleaza functia de callback
  // cu parametrul (starea / state-ul) pe care vrea sa-l trimita componentei parinte
  const liftStateUp = () => {
    callback(childState);
  };

  return (
    <Components.StyledDiv>
      <Components.StyledParagraph>Child component</Components.StyledParagraph>
      <Components.StyledButton onClick={liftStateUp}>
        Lift state up
      </Components.StyledButton>
    </Components.StyledDiv>
  );
};

export default AboutChildComponent;

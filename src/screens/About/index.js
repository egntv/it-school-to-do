import { useContext, useEffect, useState } from "react";
import { UserContext } from "../../providers/UserProvider/context";
import { Components } from "./styled";

const About = () => {
  const { users, setUsers } = useContext(UserContext);
  const [totalUsers, setTotalUsers] = useState();

  useEffect(() => {
    fetchData().then();
  }, []);

  const fetchData = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/users", {
      method: "GET",
    });
    const parsedResponse = await response.json();
    const filteredData = filterCompletedData(parsedResponse);
    const sortedData = sortDataById(filteredData);
    const total = calculateTotal(sortedData);
    setTotalUsers(total);
  };

  const filterCompletedData = (data) => {
    return data?.map((item) => {
      const processedName = item?.name + item?.username + item?.email;
      const processedAddress =
        item?.address?.city + item?.address?.street + item?.address?.suite;
      return { ...item, processedAddress, processedName };
    });
  };

  const sortDataById = (data) => {
    return data.sort((a, b) => b.id - a.id);
  };

  const calculateTotal = (data) => {
    return data.length;
  };

  return (
    <Components.StyledDiv>
      <Components.StyledParagraph>About</Components.StyledParagraph>
      {users?.map((user) => (
        <Components.ListItem key={user?.id}>{user?.name}</Components.ListItem>
      ))}
    </Components.StyledDiv>
  );
};

export default About;

import React from "react";
import "./TodoSearch.css";

const TodoSearch = ({ updateValue }) => {
  const onChange = (event) => {
    updateValue(event?.target?.value);
  };

  return (
    <form className="search-container">
      <input className="search-input" onChange={onChange} />
      <button className="search-button">Add Todo</button>
    </form>
  );
};

export default React.memo(TodoSearch);

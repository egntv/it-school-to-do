import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import { TodosContext } from "../../providers/TodosProvider/context";

const TodoContainer = styled.div`
  width: 100%;
  padding-left: 0px;
`;

const TodoItem = styled.div`
  ${(p) => (p?.isFinished ? "border: 5px solid black" : "")};
  background-color: darkorange;
  padding: 20px;
  margin-bottom: 10px;

  border-radius: 10px;
`;

const TodoList = ({ myTodos, searchValue }) => {
  const { todos, setTodos } = useContext(TodosContext);
  const [list, setList] = useState();

  useEffect(() => {
    const filteredList = filterTodos();
    setList(filteredList);
  }, [searchValue]);

  useEffect(() => {
    setList(todos);
  }, [todos]);

  const filterTodos = useCallback(() => {
    return todos?.filter((todo) => todo.title.includes(searchValue));
  }, []);

  const deleteItem = (id) => {
    setList(list.filter((item) => item.id !== id));
  };

  return (
    <TodoContainer>
      {(list || [])?.map((todo) => (
        <>
          <TodoItem key={todo?.id} isFinished={todo?.completed}>
            {todo?.title}
          </TodoItem>
          <button onClick={() => deleteItem(todo?.id)}>delete</button>
        </>
      ))}
    </TodoContainer>
  );
};

export default TodoList;

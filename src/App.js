import React, { useState } from "react";
import TodoList from "./components/TodoList/TodoList";
import TodoSearch from "./components/TodoSearch/TodoSearch";

import { todos } from "./utils/todos";

const App = () => {
  const [value, setValue] = useState();

  const updateValue = (newValue) => {
    setValue(newValue);
  };

  return (
    <div className="app">
      <TodoSearch updateValue={updateValue} />
      <TodoList myTodos={todos} searchValue={value} />
    </div>
  );
};

export default App;
